import {FETCH_MESSAGES, SEND_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE } from '../constants'

export function fetchMessages(data){
    return {
        type: FETCH_MESSAGES,
        payload: data
    }
}

export function sendMessage(data) {
    return {
        type: SEND_MESSAGE,
        payload: data
    }
}

export function editMessage(id, message) {
    return {
        type: EDIT_MESSAGE,
        payload: {id: id, message: message}
    }
}

export function deleteMessage(id) {
    return {
        type: DELETE_MESSAGE,
        payload: id
    }
}