import {FETCH_MESSAGES, SEND_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE } from '../constants';
import {getCurrentTime} from '../utils/getCurrentTime';

const messagesState = {
    messages: [],
    currentUser: 'Andrew'
};

export default (state = messagesState, action ) => {
  const {type, payload} = action;
    switch (type) {
      case FETCH_MESSAGES:
          return {...state, messages: payload};
        case SEND_MESSAGE:
            let newMessage = {
                         id: (parseInt(state.messages[state.messages.length - 1].id) + 10).toString(),
                         user: state.currentUser,
                         created_at: getCurrentTime(),
                         message: payload
                     };
            return {...state, messages: [...state.messages, newMessage]};
        case EDIT_MESSAGE:
            let usersEditMessage = [...state.messages];
            usersEditMessage.forEach(message => {
               if (message.id === payload.id) {
                   message.message = payload.message;
               }
            });
            return {...state, messages: usersEditMessage};
        case DELETE_MESSAGE:
            let userDeleteId;
            let usersDeleteMessage = [...state.messages];
            usersDeleteMessage.forEach((message, index) => {
                if (message.id === payload){
                    userDeleteId = index;
                }
            });
            usersDeleteMessage.splice(userDeleteId,1);
            return {...state, messages: usersDeleteMessage};
      default:
          return state;
  }
};