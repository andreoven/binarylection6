import React from 'react';
import Chat from './Chat';
import Spinner from './Spinner';
import { connect } from 'react-redux';
import { fetchMessages } from './actions';

class App extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isLoaded: false
        }
    }

    componentDidMount() {
        const {fetchMessages} = this.props;
        fetch('https://api.myjson.com/bins/1hiqin')
        .then(response => response.json())
        .then(data => {
            fetchMessages(data);
            this.setState({isLoaded: true})
        }
        );
    }

    render() {
        if (this.state.isLoaded) {
             return (<Chat />);
        } else {
            return (<Spinner/>);
        }
    }
}

export default connect(null, {fetchMessages})(App);