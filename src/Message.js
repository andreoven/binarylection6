import React from 'react';
import Line from "./Line";
import {connect} from 'react-redux';
import { editMessage, deleteMessage } from './actions';

class Message extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            isEdit: false,
            like: 'Like'
        }
    }

    onChange(e) {
        this.setState({
            text: e.target.value
        })
    }

    onEdit(message) {
        this.setState({
            text: message,
            isEdit: !this.state.isEdit
        })
    }

    closeInput(id, message) {
        const {editMessage} = this.props;
        editMessage(id, message);
        this.setState({isEdit: !this.state.isEdit})

    }

    likeMessage(){
        if (this.state.like === 'Like') {
            this.setState({
                like: 'Liked'
            })
        } else {
            this.setState({
                like: 'Like'
            })
        }
    }

    onDelete(id) {
        const {deleteMessage} = this.props;
        deleteMessage(id);
    }

    render() {
        const {user, isSeparator, currentUser, } = this.props;
        if (user.user === currentUser) {
            if (this.state.isEdit) {
                return (
                    <div id="myModal" className="modal">
                        <div className="modal-content">
                            <p>Edit message</p>
                            <input type="text" value={ this.state.text } onChange={ (e) => this.onChange(e) }/>
                            <p onClick={() => this.closeInput(user.id, this.state.text)}>Save</p>
                        </div>
                    </div>
                );
            } else {
                return (
                    <div className="message">
                        <Line separator = {isSeparator} />
                        <div className="message-main">
                            <div className="message-image">
                                <img src={user.avatar} alt=""/>
                            </div>
                            <div className="message-data">
                                <div className="message-user">
                                    <p>Name: {user.user}</p>
                                    <p>Date: {user.created_at}</p>
                                </div>
                                <div className="message-text">
                                    <p>Message: {user.message}</p>
                                    <div className="message-edit">
                                        <p onClick={() => this.onDelete(user.id)}> Delete</p>
                                        <p className="edit-show" onClick={() => this.onEdit(user.message)}>Edit</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            }
        }  else {
            return (
                <div className="message">
                    <Line separator = {isSeparator} />
                    <div className="message-main">
                        <div className="message-image">
                            <img src={user.avatar} alt=""/>
                        </div>
                        <div className="message-data">
                            <div className="message-user">
                                <p>Name: {user.user}</p>
                                <p>Date: {user.created_at}</p>
                            </div>
                            <div className="edit">
                                <p>Message: {user.message}</p>
                                <p onClick={() => this.likeMessage()}>{this.state.like}</p>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

export default connect(null, {editMessage, deleteMessage})(Message);