import React from 'react';
import Header from "./Header";
import MessageList from "./MessageList";
import MessageInput from "./MessageInput";
import {connect} from 'react-redux'


class Chat extends React.Component {

    getHeaderInfo(users) {
        let usersCount = new Set();
            users.forEach(user => {
               usersCount.add(user.user);
            });
        let messageCount = users.length;
            return {
                users: usersCount.size,
                messages: messageCount,
                lastMessage: users[users.length - 1].created_at
            }
    }

    render() {
        let headerInfo = this.getHeaderInfo(this.props.messages);
        return (
            <div className="Chat">
                <Header headerInfo = {headerInfo} />
                <MessageList users = {this.props.messages} currentUser = {this.props.currentUser}  />
                <MessageInput  />
            </div>
        );
    }
}

export default connect(
    (state) => ({
        messages: state.messages.messages,
        currentUser: state.messages.currentUser
    }))(Chat);